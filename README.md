# UnicornMedical

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Code Review
> by Christian Kusmanow <christian.kusmanow@teslasoft, hans.christian.kusmanow@gmail.com>

## Source Code Project
* Common IDE settings are missing.
* No code formatting settings available.
* Git repository merge strategy has not been configured.
  Fast forward merging is advised for enterprise projects by executing `git config pull.rebase true` on repository setup.
  Every enterprise developer shall have this enabled by default by executing `git config --global pull.rebase true` on his development machine.
  > This is the only way to avoid foxtrot merges!
  > http://bit-booster.blogspot.com/2016/02/no-foxtrots-allowed.html
  
## Code Base
* A procedural code style has been used instead of using a modern OOP code style.
* Does not have a design pattern structure instead it has the a common framework demo application layout for very small individual projects.
* Is framework driven and not code base driven.
* No proper namespacing available. Everything is referenced relatively. (tsconfig)
* The application will not survive the next framework exchange.

## Code Style
* Inconsistent code styling in almost all files and types.
* Mix of object oriented pascal case (object names) and procedural camel case (members and functions).
* Because of the usage of camel case there is no distinguishing between framework code and enterprise code possible.
* File naming is inconsistent to the containing objects and represents the default angular project code style.
* All these flaws are eliminating intelligent IntelliSense support in IDEs. (i.e. sorting of most relevant object members)

## JavaScript Transpilation
* Core-JS has not been imported properly and polyfills only the metadata reflection API but does not also polyfill standard ECMAScript methods. (core-js/client/shim)
* The application potentially targets insecure IE9, IE10 browsers by providing commented polyfill includes.
* Primary compiles to ES5 for no reason. 2018 was the year the majority of the web apps can get rid of the module bundlers or transpilers.
  Instead ES5 transpilation should be done separately for old browsers (~3%) using browser detection algorithms.
* TSLint Import blacklist contains old fashion exclude for rxjs. But now, RxJS imports are streamlined as one might have imagined from the beginning.
  This is using ES modules with full tree shaking support, just as efficient as the old way but without the manual work.

## CSS
* Some used classes are not present.
* No CSS framework for SASS like Compass used.
  Instead common SASS mixins have been manually added to the project.
* No responsive CSS framework used.
* No source maps for css available. Everything gets generated into the header.
* Sass class nesting has not been almost used.
* Bootstrap theming has not been used, instead everything like colors is overridden.
* App theming impossible: Color names and other properties have been directly used instead translating 
  them into component states using a separate theme file.
* Almost all application component styles are located within the `assets/sass/partials` directory which
  should only be used to customize css framework components or common layout and typography elements.

# Angular testing made easy using *TestBed* generation and auto mocking

## The pain of testing angular components
Angular testing is a pain and it makes from a professional perspective no sense to duplicate module definitions and 
to fight against the whole aggregation chain of defined modules components and services which results in the double 
of effort to maintain the application logic and testing code and makes quick changes on module definitions impossible,
until all effected component test have been updated reflecting these changes in order not to break the stability of 
existing test code.

## Which is the optimal approach for testing angular? 
The approach of blacklisting and manual mocking of everything is a tremendous development performance killer.
The optimal scenario of testing application components is the re-usage of existing module definitions,
and to drive the whitelisting approach which means you pick only that what is needed for the your test.
The technique for this is called Shallow Rendering and there is currently only one package for angular in the os world
available which is called [shallow-render](https://github.com/getsaf/shallow-render) and does the job efficiently.

## Preconditions for *shallow-renderer*
In order to be able to use the package *shallow-render* (auto mocking) for easy angular testing by using a 
whitelisting technique for mock exclusions, it is necessary to upgrade to a Angular version of >= 6.0.

### Steps for Upgrading to the latest Angular 7 version
Now that Angular has committed to bi-annual upgrades, from a security perspective taking your app to Angular 7 is a must.

* Simply use a tool for upgrading all npm dependencies to the latest revision. Be aware to upgrade to a minimum npm 
version of >= 8 because the build processes require them and the upgrade would left out and remove not buildable 
packages.
  ```
  yarn global add yarn-upgrade-all
  yarn-upgrade-all
  ```
* Upgrade Angular 4 framework components using the angular cli.
  This will update everything necessary to build, run and test Angular applications:
  ```
  ng update @angular/cli --migrate-only --from=4.0.0
  ```
* Clean up all unmet peer dependencies by watching the npm install process output.
* Finally remove all deprecated RxJS 6 features by using the package *rxjs-tslint* to prepare the application for 
  future development:
  ```
  rxjs-5-to-6-migrate -p src/tsconfig.app.json
  ```
* Fix renamed angular 4 modules:
  * Switch from `Http` to `HttpClient` and the module import to `HttpClientModule`.
  * Switch from `Response` to `HttpResponse<T>`.

Congratulations, you are developing now with the latest angular version.
