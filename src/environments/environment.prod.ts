export const environment = {
    production         : true,
    searches           : [
        'TypeScript',
        'Angular',
        'C#'
    ],
    searchDebounceDelay: false
};
