import {AppModule} from 'app';
import {WeatherSearchServiceProvider} from 'search/providers';
import {WeatherSearchServiceMock} from 'search/services/WeatherSearch';
import {WeatherSearchComponent} from 'search/view';
import {WeatherSearchViewModel} from 'search/view-model';
import {Shallow} from 'shallow-render';


describe('WeatherSearchComponentComponent', () =>
{
    let shallow: Shallow<WeatherSearchComponent>;

    beforeEach(() =>
    {
        shallow = new Shallow(WeatherSearchComponent, AppModule);
        shallow.dontMock(
            WeatherSearchViewModel,
            WeatherSearchServiceMock
        );
        shallow.provide(
            WeatherSearchServiceProvider.Staging
        );
    });

    it('should create', async () =>
    {
        const {outputs} = await shallow.render();
        expect(outputs).toBeTruthy();
    });
});
