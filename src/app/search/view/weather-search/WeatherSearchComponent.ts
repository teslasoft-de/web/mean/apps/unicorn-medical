import {ChangeDetectionStrategy, Component} from '@angular/core';

import {ComponentBase} from 'base/view';
import {WeatherSearchViewModel} from 'search/view-model';


@Component({
    selector       : 'app-weather-search-component',
    templateUrl    : './WeatherSearchComponent.html',
    styleUrls      : ['./WeatherSearchComponent.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers      : [WeatherSearchViewModel]
})
export class WeatherSearchComponent extends ComponentBase<WeatherSearchViewModel>
{
    constructor(vm: WeatherSearchViewModel)
    {
        super(vm);
    }
}
