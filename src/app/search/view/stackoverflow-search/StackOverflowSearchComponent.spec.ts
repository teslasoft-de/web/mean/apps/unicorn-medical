import {AppModule} from 'app';
import {StackOverflowSearch} from 'search/model/StackOverflowSearch';
import {SearchServiceProvider} from 'search/providers';
import {SearchServiceMock} from 'search/services/StackOverflowSearch';
import {StackOverflowSearchComponent} from 'search/view';
import {StackOverflowSearchViewModel} from 'search/view-model';
import {Shallow} from 'shallow-render';


describe('SearchComponent', () =>
{
    let shallow: Shallow<StackOverflowSearchComponent>;

    beforeEach(() =>
    {
        shallow = new Shallow(StackOverflowSearchComponent, AppModule);
        shallow.dontMock(
            StackOverflowSearchViewModel,
            SearchServiceMock
        );
        shallow.provide(
            SearchServiceProvider.Staging
        );
    });

    it('should create', async () =>
    {
        const {outputs} = await shallow.render();
        expect(outputs).toBeTruthy();
    });

    it('should perform vm.ToggleSpinner and fire vm.SearchEvent', async (done) =>
    {
        const {instance} = await shallow.render();

        instance.Model = new StackOverflowSearch('something');
        instance.vm.Model.SearchEvent.Subscribe(() =>
        {
            expect(instance.vm.Model.SearchResult.length).toBeGreaterThan(0);
            done();
        });
        instance.vm.SearchAction();
    });
});
