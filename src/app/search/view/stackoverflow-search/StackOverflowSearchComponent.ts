import {Component, Input, ViewChild} from '@angular/core';
import {ComponentBase} from 'app/base/view';

import {Command, CommandHandler} from 'base/view-model';
import {NgxSpinnerComponent, NgxSpinnerService} from 'ngx-spinner';
import {SearchResultEventArgs} from 'search/model';
import {SearchResultItem, StackOverflowSearch} from 'search/model/StackOverflowSearch';
import {StackOverflowSearchViewModel} from 'search/view-model';


@Component({
    selector   : 'app-stackoverflow-search',
    templateUrl: './StackOverflowSearchComponent.html',
    styleUrls  : ['./StackOverflowSearchComponent.scss'],
    providers  : [StackOverflowSearchViewModel]
})
export class StackOverflowSearchComponent<T extends SearchResultItem = T> extends ComponentBase<StackOverflowSearchViewModel>
{
    @ViewChild(NgxSpinnerComponent)
    private SearchSpinner: NgxSpinnerComponent;

    @Input('model')
    public set Model(value: StackOverflowSearch)
    {
        this.vm.Model                  = value;
        (<any>this.SearchSpinner).name = `${value.Name}Spinner`;
    }

    @Input('on-progress')
    public set OnProgress(value: Command<CommandHandler>)
    {
        this.vm.SearchProgressAction = value;
    }

    constructor(vm: StackOverflowSearchViewModel<T>, private _SpinnerService: NgxSpinnerService)
    {
        super(vm);
    }

    public ngAfterViewInit()
    {
        const searchProgressAction   = this.vm.SearchProgressAction;
        this.vm.SearchProgressAction = async (params: CommandHandler<SearchResultEventArgs<T>>) =>
        {
            await this.ToggleSpinner(params);
            searchProgressAction && await searchProgressAction(params);
        };
        super.ngAfterViewInit();
    }

    public async ToggleSpinner(params: CommandHandler<SearchResultEventArgs<T>>)
    {
        const name = `${this.vm.Model.Name}Spinner`;
        if (params.eventArgs == SearchResultEventArgs.Empty) {
            await this._SpinnerService.hide(name);
        } else if (params.eventArgs.SearchResultItems == null) {
            await this._SpinnerService.show(name);
        }
    }
}
