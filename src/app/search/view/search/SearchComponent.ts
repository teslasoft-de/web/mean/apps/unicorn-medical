import {Component, QueryList, ViewChild, ViewChildren} from '@angular/core';

import {ComponentBase} from 'base/view';
import {SearchViewModel, StackOverflowSearchViewModel} from 'search/view-model';
import {StackOverflowSearchComponent} from 'search/view/stackoverflow-search/StackOverflowSearchComponent';
import {WeatherSearchComponent} from 'search/view/weather-search/WeatherSearchComponent';


@Component({
    selector   : 'app-search',
    templateUrl: './SearchComponent.html',
    styleUrls  : ['./SearchComponent.scss'],
    providers  : [SearchViewModel]
})
export class SearchComponent extends ComponentBase<SearchViewModel>
{
    @ViewChildren(StackOverflowSearchComponent)
    public set StackOverflowSearches(value: QueryList<StackOverflowSearchComponent>)
    {
        this.vm.SearchViewModels = value.toArray().map<StackOverflowSearchViewModel>(search => search.vm);
    }

    @ViewChild(WeatherSearchComponent)
    public set WeatherSearches(value: WeatherSearchComponent)
    {
        this.vm.WeatherSearchViewModel = value.vm;
    }

    public get ProgressColumnSpacing(): string
    {
        return `col-sm-${12 / (this.vm.SearchResults.size < 5 ? this.vm.SearchResults.size : 4)}`;
    }

    constructor(vm: SearchViewModel)
    {
        super(vm);
    }
}
