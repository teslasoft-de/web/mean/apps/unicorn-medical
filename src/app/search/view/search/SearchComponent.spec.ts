import {AppModule} from 'app';
import {TabDirective, TabsetComponent} from 'ngx-bootstrap';
import {SearchComponent, StackOverflowSearchComponent} from 'search/view';
import {SearchViewModel} from 'search/view-model';
import {Shallow} from 'shallow-render';


describe('SearchComponent', () =>
{
    let shallow: Shallow<SearchComponent>;

    beforeEach(() =>
    {
        shallow = new Shallow(SearchComponent, AppModule);
        shallow.dontMock(
            SearchViewModel,
            TabsetComponent,
            TabDirective
        );
        shallow.mock(StackOverflowSearchComponent, {
            vm: <any>{
                async SearchAction() {}
            }
        });
    });

    it('should create', async () =>
    {
        const {outputs} = await shallow.render();
        expect(outputs).toBeTruthy();
    });

    it('should fire SearchEvent', async () =>
    {
        const {fixture, instance} = await shallow.render();
        const searchViewModel     = instance.StackOverflowSearches.toArray()[0].vm;

        spyOn(searchViewModel, 'SearchAction');
        const button = fixture.debugElement.nativeElement.querySelector('button');
        button.click();

        fixture.detectChanges();
        expect(searchViewModel.SearchAction).toHaveBeenCalled();
    });
});
