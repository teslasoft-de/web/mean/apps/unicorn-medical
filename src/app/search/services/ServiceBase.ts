import {ServiceBase as Base} from 'base/services';
import {Observable} from 'rxjs';
import {SearchResultItemBase} from 'search/model';
import {SearchServiceException} from 'search/services/SearchServiceException';


/**
 * Represents the basic search behavior for any search service.
 * Accepts only one search for a specific keyword at a time.
 */
export abstract class ServiceBase<T extends SearchResultItemBase> extends Base<Array<T>>
{
    /**
     * Performs the search, checks for invalid parameters and monitors the search progress state.
     * @param workerName
     * @throws SearchServiceException
     */
    public Search(workerName: string): Observable<Array<T>>
    {
        if (workerName == null || workerName == '') {
            throw new SearchServiceException('The keyword cannot be null or empty.');
        }
        if (this._BackgroundWorkers.has(workerName)) {
            throw new SearchServiceException(`A '${workerName}' search is already in progress.`);
        }

        return this.CreateBackgroundWorker(this.DoSearch(workerName), workerName);
    }

    /**
     * Implement in the derived class to perform the search and return the search result.
     * @param keyword
     */
    protected abstract DoSearch(keyword: string): Observable<Array<T>>;

    public static HandleError(e: Error)
    {
        if (SearchServiceException.TypeOf(e)) {
            console.error('Search service exception thrown:', e);
        } else {
            console.error('Unhandled exception during search thrown:', e);
        }
    }
}
