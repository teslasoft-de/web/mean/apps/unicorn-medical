import {Exception} from 'base/model';


/**
 * Represents the default exception domain for the `SearchService`.
 */
export class SearchServiceException extends Exception
{
    public get ResponseBody(): JSON
    {
        return this._responseBody;
    }

    constructor(message: string, private _responseBody?: JSON)
    {
        super(message);

        SearchServiceException.SetPrototype(this);
    }
}
