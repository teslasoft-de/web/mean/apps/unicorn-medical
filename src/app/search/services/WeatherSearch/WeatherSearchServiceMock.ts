import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import {IWeatherSearchService, WeatherSearchResultItem} from 'search/model/WeatherSearch';
import {ServiceBase} from 'search/services';


export interface IWeatherSearchResponse
{
    [p: string]: string | number
}

@Injectable({
    providedIn: 'root'
})
export class WeatherSearchServiceMock extends ServiceBase<WeatherSearchResultItem> implements IWeatherSearchService
{
    constructor()
    {
        super();
    }

    protected DoSearch(keyword: string): Observable<Array<WeatherSearchResultItem>>
    {
        const entries = Array();
        const subject = new BehaviorSubject(entries);
        import('./weatherdata.json').then((data: { default: Array<IWeatherSearchResponse> }) =>
        {
            const itemCount = data.default.length;
            let timeout     = 0;
            for (let i = 0; i < itemCount; i++) {
                // Debounce item generation by creating items block wise.
                if (!(i % 100) || i == itemCount - 1) {
                    setTimeout(() =>
                    {
                        for (let j = entries.length; j < i; j++) {
                            entries.push(WeatherSearchResultItem.FromJSON(data.default[j]));
                        }
                        subject.next(entries);
                        if (i == itemCount - 1) {
                            subject.complete();
                        }
                    }, timeout += 50);
                }
            }
        });
        return subject.asObservable();
    }
}
