import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {IWeatherSearchService, WeatherSearchResultItem} from 'search/model/WeatherSearch';
import {WeatherSearchServiceProvider} from 'search/providers';
import {ServiceBase} from 'search/services';


export function SearchServiceFactory(provider: WeatherSearchServiceProvider)
{
    return provider.Provide(WeatherSearchService);
}

/**
 * Represents the concrete production search service which is doing real web request against a public API url.
 */
@Injectable({
    providedIn: 'root',
    useFactory: SearchServiceFactory,
    deps      : [WeatherSearchServiceProvider]
})
export class WeatherSearchService<T extends WeatherSearchResultItem = T> extends ServiceBase<T> implements IWeatherSearchService
{
    protected DoSearch(keyword: string): Observable<Array<T>>
    {
        return undefined;
    }
}
