import {Injectable} from '@angular/core';

import {environment} from 'environments/environment';
import {Observable, Subscriber} from 'rxjs';
import {SearchResultItem} from 'search/model/StackOverflowSearch';
import {ServiceBase} from 'search/services';


/**
 * Represents a mocked search service and simulates asynchronous search service results
 * used to develop the view within the application design stage and for unit testing.
 */
@Injectable({
    providedIn: 'root'
})
export class SearchServiceMock extends ServiceBase<SearchResultItem>
{
    protected DoSearch(keyword: string): Observable<Array<SearchResultItem>>
    {
        const itemCount  = 20,
              createItem = (i: number) =>
              {
                  const time = Date.now() / 1000;
                  return new SearchResultItem(
                      i,
                      time,
                      'closed_reason',
                      time,
                      !!(i % 2),
                      time,
                      'link',
                      i,
                      [`tag${i + 1}`, `tag${i + 2}`, `tag${i + 3}`, `tag${i + 4}`],
                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                      i
                  );
              };

        return new Observable((observer: Subscriber<Array<SearchResultItem>>) =>
        {
            const items  = <Array<SearchResultItem>>[],
                  random = Math.random();
            let timeout  = 0;

            for (let i = 0; i < itemCount; i++) {
                setTimeout(() =>
                {
                    items.push(createItem(i));

                    if (environment.searchDebounceDelay) {
                        observer.next(items);
                    }

                    if (i == itemCount - 1) {
                        if (!environment.searchDebounceDelay) {
                            observer.next(items);
                        }
                        observer.complete();
                    }
                }, timeout);

                if (environment.searchDebounceDelay) {
                    // debounce item generation by using the completion percentage as acceleration ratio.
                    timeout += random * 100 * (5 - (5 * (i / itemCount)));
                }
            }
            return {unsubscribe() {}};
        });
    }
}
