import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {map} from 'rxjs/operators';
import {ISearchService, SearchResultItem} from 'search/model/StackOverflowSearch';
import {SearchServiceProvider} from 'search/providers';
import {ServiceBase} from 'search/services';


export interface ISearchResponse
{
    has_more: boolean,

    items: Array<ISearchResultItem>,

    /**
     * requests available
     */
    quota_remaining: number;

    /**
     * requests maximum
     */
    quota_max: number;
}

export interface ISearchResultItem
{
    answer_count: number;
    closed_date: number;
    closed_reason: string;
    creation_date: number;
    is_answered: boolean;
    last_activity_date: number;
    link: string;
    score: number;
    tags: Array<string>;
    title: string;
    view_count: number;
}

export function SearchServiceFactory(provider: SearchServiceProvider)
{
    return provider.Provide(SearchService);
}

/**
 * Represents the concrete production search service which is doing real web request against a public API url.
 */
@Injectable({
    providedIn: 'root',
    useFactory: SearchServiceFactory,
    deps      : [SearchServiceProvider]
})
export class SearchService extends ServiceBase<SearchResultItem> implements ISearchService
{
    private static readonly apiUrl =
                                'https://api.stackexchange.com/2.2/search?pagesize=20&order=desc&sort=activity&site=stackoverflow&intitle=';

    constructor(private http: HttpClient)
    {
        super();
    }

    protected DoSearch(keyword: string)
    {
        return this.http.get(SearchService.apiUrl + keyword).pipe(
            map((res: ISearchResponse) =>
            {
                const data = res.items;
                console.log('API USAGE: ' + res.quota_remaining + ' of ' + res.quota_max + ' requests available');

                return data.map(item => new SearchResultItem(
                    item.answer_count,
                    item.closed_date,
                    item.closed_reason,
                    item.creation_date,
                    item.is_answered,
                    item.last_activity_date,
                    item.link,
                    item.score,
                    item.tags,
                    item.title,
                    item.view_count
                ));
            })
        );
    }
}
