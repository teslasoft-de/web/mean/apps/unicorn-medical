import {HttpClientModule} from '@angular/common/http';
import {inject, TestBed} from '@angular/core/testing';

import {Exception} from 'base/model';
import {SearchServiceProvider} from 'search/providers';
import {SearchService} from 'search/services/StackOverflowSearch';


describe('SearchService', () =>
{
    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            imports  : [HttpClientModule],
            providers: [
                SearchServiceProvider.Staging
            ]
        });
    });

    it('should create SearchService', inject([SearchService], (service: SearchService) =>
    {
        expect(service).toBeTruthy();
    }));

    function search(service: SearchService, keyword: any)
    {
        let exception: Exception;
        try {
            service.Search(keyword);
        } catch (e) {
            exception = e;
        }
        return exception;
    }

    it('should throw on null keyword', inject([SearchService], (service: SearchService) =>
    {
        const exception = search(service, null);

        expect(exception).toBeTruthy();
    }));

    it('should throw on empty keyword', inject([SearchService], (service: SearchService) =>
    {
        const exception = search(service, '');

        expect(exception).toBeTruthy();
    }));

    it('should throw on double search', inject([SearchService], (service: SearchService) =>
    {
        const keyword = 'something';
        let exception = search(service, keyword);
        expect(exception).toBeUndefined('Test invalidated due to first search already failed.');
        exception = search(service, keyword);

        expect(exception).toBeTruthy();
    }));
});
