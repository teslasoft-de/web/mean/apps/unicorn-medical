import {HttpClient} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';

import {IServiceProvider, ProviderBase} from 'base/providers';
import {SearchServiceMock} from '../services/StackOverflowSearch/SearchServiceMock';


/**
 * Provides the staging configuration for the search service.
 */
@Injectable({
    providedIn: 'root'
})
export class SearchServiceProvider extends ProviderBase
{
    constructor(parentInjector: Injector)
    {
        super(parentInjector);
    }

    public ProductionFactory(): IServiceProvider
    {
        return {
            deps: [HttpClient]
        };
    }

    public DevelopmentFactory(): IServiceProvider
    {
        return {
            provide: SearchServiceMock
        };
    }
}
