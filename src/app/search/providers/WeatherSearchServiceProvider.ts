import {Injectable, Injector} from '@angular/core';

import {IServiceProvider, ProviderBase} from 'base/providers';
import {WeatherSearchServiceMock} from '../services/WeatherSearch/WeatherSearchServiceMock';


/**
 * Provides the staging configuration for the search service.
 */
@Injectable({
    providedIn: 'root'
})
export class WeatherSearchServiceProvider extends ProviderBase
{
    constructor(parentInjector: Injector)
    {
        super(parentInjector);
    }

    public ProductionFactory(): IServiceProvider
    {
        return {};
    }

    public DevelopmentFactory(): IServiceProvider
    {
        return {
            provide: WeatherSearchServiceMock
        };
    }
}
