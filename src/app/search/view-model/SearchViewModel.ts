import {IEvent} from 'base/model';
import {Command, CommandHandler, IViewModel, ViewModelBase} from 'base/view-model';
import {BehaviorSubject, Observable} from 'rxjs';
import {Search, SearchResultEventArgs} from 'search/model';
import {SearchResultItem, StackOverflowSearch} from 'search/model/StackOverflowSearch';
import {SearchService} from 'search/services/StackOverflowSearch';
import {WeatherSearchService} from 'search/services/WeatherSearch';
import {StackOverflowSearchViewModel, WeatherSearchViewModel} from 'search/view-model';


export interface ISearchViewModel<T extends SearchResultItem = T, TArgs extends SearchResultEventArgs<T> = TArgs>
    extends IViewModel<Search>
{
    SearchViewModels: Array<StackOverflowSearchViewModel>
    IsSearchInProgress: boolean
    SearchProgressPercentage: number

    WeatherSearchViewModel: WeatherSearchViewModel
    IsWeatherSearchInProgress: boolean
    WeatherSearchProgressPercentage: Observable<number>

    SearchResults: Map<string, Array<T>>

    StackOverflowSearchAction: Command<CommandHandler>
    WeatherSearchAction: Command<CommandHandler>
    SearchProgressAction: Command<CommandHandler<TArgs>>

    StackOverflowSearch_Done: IEvent<StackOverflowSearch, TArgs>
}

export class SearchViewModel<T extends SearchResultItem = T, TArgs extends SearchResultEventArgs<T> = TArgs>
    extends ViewModelBase<Search> implements ISearchViewModel<T, TArgs>
{
    private _Search                   = new Search();
    private _WeatherSearchViewModel: WeatherSearchViewModel;
    private _WeatherSearchPercentage  = new BehaviorSubject(0);
    private _WeatherSearchPercentage$ = this._WeatherSearchPercentage.asObservable();
    private _SearchViewModels: Array<StackOverflowSearchViewModel<T, TArgs>>;
    private _SearchResults            = new Map<string, Array<T>>();

    //region Public Properties

    public get Name(): string
    {
        return 'Search Komponente...';
    }

    public get Model()
    {
        return this._Search;
    }

    public set SearchViewModels(value: Array<StackOverflowSearchViewModel<T>>)
    {
        this._SearchViewModels = value;
        this._SearchViewModels.forEach(viewModel =>
        {
            viewModel.Model.SearchEvent.Subscribe(this.StackOverflowSearch_Done, this);
        });
    }

    public set WeatherSearchViewModel(value: WeatherSearchViewModel)
    {
        this._WeatherSearchViewModel = value;
    }

    public get IsWeatherSearchInProgress(): boolean
    {
        return !!this._WeatherSearchService.BackgroundWorkers.size;
    }

    public get WeatherSearchProgressPercentage(): Observable<number>
    {
        return this._WeatherSearchPercentage$;
    }

    public get IsSearchInProgress(): boolean
    {
        return !!this._SearchService.BackgroundWorkers.size;
    }

    public get SearchProgressPercentage(): number
    {
        const results  = Array.from(this._SearchResults.values());
        const maxItems = results.length * 20;
        return results.length ? results.reduce((sum, current) => sum + current.length, 0) / maxItems * 100 : 0;
    }

    public get SearchResults(): Map<string, Array<T>>
    {
        return this._SearchResults;
    }

    //endregion

    constructor(private _SearchService: SearchService, private _WeatherSearchService: WeatherSearchService)
    {
        super();
    }

    public Destroy(): void
    {
        this._SearchViewModels && this._SearchViewModels.forEach(viewModel =>
        {
            viewModel.Model.SearchEvent.Unsubscribe(this.StackOverflowSearch_Done);
        });
        this._Search.Dispose();
    }

    public WeatherSearchAction = async () =>
    {
        const subscription = this._WeatherSearchViewModel.Model.NextResult.subscribe(result =>
        {
            this._WeatherSearchPercentage.next(result.length ? result.length / 5760 * 100 : 0);
        });
        (await this._WeatherSearchViewModel.SearchAction()).subscribe({
            complete: () =>
            {
                subscription.unsubscribe();
            }
        });
    };

    public StackOverflowSearchAction = async () =>
    {
        for (const _vm of this._SearchViewModels) {
            this._SearchResults.delete(_vm.Model.Name);
            await _vm.SearchAction();
        }
    };

    public SearchProgressAction = (params: CommandHandler<TArgs>) =>
    {
        if (params.eventArgs.SearchResultItems) {
            this._SearchResults.set(params.eventArgs.SearchName, params.eventArgs.SearchResultItems);
        }
    };

    public StackOverflowSearch_Done(sender: StackOverflowSearch<T>, ea: SearchResultEventArgs<T>)
    {
        console.log(`'${sender.Name}' search done`, ea, this);
    }
}
