import {Injectable} from '@angular/core';

import {Command, CommandHandler, IViewModel, ViewModelBase} from 'base/view-model';
import {SearchResultEventArgs} from 'search/model';
import {SearchResultItem, StackOverflowSearch} from 'search/model/StackOverflowSearch';
import {SearchService} from 'search/services/StackOverflowSearch';


/**
 * View contract interface for the `StackOverflowSearchViewModel`.
 */
export interface IStackOverflowSearchViewModel<T extends SearchResultItem = T, TArgs extends SearchResultEventArgs<T> = TArgs>
    extends IViewModel<StackOverflowSearch<T>>
{
    Name: string;
    IsSearchInProgress: boolean;
    SearchAction: Command<CommandHandler<TArgs>, Array<T>>
    SearchProgressAction: Command<CommandHandler<TArgs>>
}

@Injectable()
export class StackOverflowSearchViewModel<T extends SearchResultItem = T, TArgs extends SearchResultEventArgs<T> = TArgs>
    extends ViewModelBase<StackOverflowSearch<T>> implements IStackOverflowSearchViewModel<T>
{
    private _SearchProgressAction: Command<CommandHandler<TArgs>>;

    //region Public Properties

    public get Name(): string
    {
        return 'StackoverflowSearch Komponente...';
    }

    public get SearchProgressAction(): Command<CommandHandler<TArgs>>
    {
        return this._SearchProgressAction;
    }

    public set SearchProgressAction(value: Command<CommandHandler<TArgs>>)
    {
        this._SearchProgressAction = value;
    }

    public get IsSearchInProgress(): boolean
    {
        return this._SearchService.BackgroundWorkers.has(this.Model.Name);
    }

    //endregion

    constructor(private _SearchService: SearchService)
    {
        super();
    }

    public Init(): void
    {
        this.Model.ProgressHandler = this.OnSearchProgress.bind(this);
    }

    public SearchAction = async () =>
    {
        await this.Model.Search(this._SearchService);
    };

    protected async OnSearchProgress(sender: StackOverflowSearch<T>, progressEventArgs: TArgs)
    {
        await this._SearchProgressAction({
            sender   : this,
            eventArgs: progressEventArgs
        });
    };
}
