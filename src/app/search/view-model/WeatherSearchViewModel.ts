import {Injectable} from '@angular/core';

import {ViewModelBase} from 'base/view-model';
import {WeatherSearch, WeatherSearchResultItem} from 'search/model/WeatherSearch';
import {WeatherSearchService} from 'search/services/WeatherSearch';


@Injectable()
export class WeatherSearchViewModel<T extends WeatherSearchResultItem = T> extends ViewModelBase<WeatherSearch<T>>
{
    public get Name(): string
    {
        return 'WeatherSearch Komponente...';
    }

    constructor(private _SearchService: WeatherSearchService)
    {
        super();
    }

    public SearchAction = async () =>
    {
        return await this.Model.Search(this._SearchService, 'City');
    };
}
