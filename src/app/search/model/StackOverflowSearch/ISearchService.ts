import {IService} from 'base/services';
import {Observable} from 'rxjs';
import {SearchResultItem} from 'search/model/StackOverflowSearch';


export interface ISearchService extends IService
{
    IsSearchInProgress: boolean;
    Search: SearchRequest;
}

export interface SearchRequest
{
    (keyword: string): Observable<Array<SearchResultItem>>
}
