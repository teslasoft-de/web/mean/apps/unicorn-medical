import {ActionObserver, EventDelegate, IEvent} from 'base/model';
import {App} from 'core/app/model/App';
import {SearchResultEventArgs} from 'search/model/SearchResultEventArgs';
import {ISearchService, SearchResultItem} from 'search/model/StackOverflowSearch';
import {ServiceBase} from 'search/services';


export class StackOverflowSearch<T extends SearchResultItem = T> extends ActionObserver<Array<T>>
{
    //region Private Members

    private _SearchResult: Array<T> = [];
    private _ProgressHandler: IEvent<this, SearchResultEventArgs<T>>;

    private readonly _SearchEvent = new EventDelegate<this, SearchResultEventArgs<T>>(this, true);

    //endregion

    //region Public Properties

    public get Name(): string
    {
        return this._Name;
    }

    public get Heading(): string
    {
        return this.GenerateSearchHeading(this._Name);
    }

    public get SearchResult(): Array<T>
    {
        return this._SearchResult;
    }

    public get SearchEvent(): EventDelegate<this, SearchResultEventArgs<T>>
    {
        return this._SearchEvent;
    }

    public set ProgressHandler(value: IEvent<this, SearchResultEventArgs<T>>)
    {
        this._ProgressHandler = value;
    }

    //endregion

    constructor(private _Name: string)
    {
        super(null, []);

        this.OnProgress = this.OnSearchProgress;
        this.OnComplete = this.OnSearch;
        this.OnError    = ServiceBase.HandleError;
    }

    protected GenerateSearchHeading(topic: string)
    {
        let heading = topic;

        if (this._SearchResult && this._SearchResult.length) {
            heading += ` (${this._SearchResult.length})`;
        }

        return heading;
    }

    public async Search(searchService: ISearchService)
    {
        this.Reset();

        return await this.Observe(
            () => App.Instance.SearchTopics<T>(searchService, this._Name),
            {
                init    : () => new SearchResultEventArgs(this._Name),
                next    : (searchResult) => new SearchResultEventArgs(this._Name, searchResult),
                complete: () => new SearchResultEventArgs(this._Name, this.ActionResultValue)
            }
        );
    }

    protected async OnSearchProgress(sender: this, ea: SearchResultEventArgs<T>)
    {
        if (ea.SearchResultItems && ea.SearchResultItems.length) {
            this._SearchResult.push(ea.SearchResultItems[ea.SearchResultItems.length - 1]);
        }
        this._ProgressHandler && await this._ProgressHandler(this, ea);
    }

    /**
     * Event invocation method for the SearchEvent.
     * @param sender
     * @param ea
     */
    protected async OnSearch(sender: this, ea: SearchResultEventArgs<T>)
    {
        this._SearchResult = ea.SearchResultItems;
        this._ProgressHandler && await this._ProgressHandler(this, SearchResultEventArgs.Empty);
        this._SearchEvent.Invoke(ea);
    }

    public Reset()
    {
        this._SearchResult = [];
        super.Reset();
    }

    /** @inheritDoc */
    public Equals(other: StackOverflowSearch<T>): boolean
    {
        return super.Equals(other) &&
               this._Name == other._Name &&
               ((!this._SearchResult && !other._SearchResult) ||
                this._SearchResult && other._SearchResult && (
                    this._SearchResult.length == other._SearchResult.length &&
                    this._SearchResult.every((item: T, index: number) =>
                    {
                        return item.Equals(other._SearchResult[index]);
                    })
                ));
    }
}
