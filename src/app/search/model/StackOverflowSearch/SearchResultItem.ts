import {unescape} from 'lodash';

import {SearchResultItemBase} from 'search/model/SearchResultItemBase';
import {ISearchResultItem} from 'search/services/StackOverflowSearch';


export class SearchResultItem extends SearchResultItemBase implements ISearchResultItem
{
    private readonly _title: string;

    constructor(
        private readonly _answer_count: number,
        private readonly _closed_date: number,
        private readonly _closed_reason: string,
        private readonly _creation_date: number,
        private readonly _is_answered: boolean,
        private readonly _last_activity_date: number,
        private readonly _link: string,
        private readonly _score: number,
        private readonly _tags: Array<string>,
        _title: string,
        private readonly _view_count: number)
    {
        super();
        this._title = unescape(_title);
    }

    //region ISearchResultItem Members

    public get answer_count(): number
    {
        return this._answer_count;
    }

    public get closed_date(): number
    {
        return this._closed_date;
    }

    public get closed_reason(): string
    {
        return this._closed_reason;
    }

    public get creation_date(): number
    {
        return this._creation_date;
    }

    public get is_answered(): boolean
    {
        return this._is_answered;
    }

    public get last_activity_date(): number
    {
        return this._last_activity_date;
    }

    public get last_activity_date_time(): string
    {
        return new Date(this._last_activity_date * 1000).toLocaleString();
    }

    public get link(): string
    {
        return this._link;
    }

    public get score(): number
    {
        return this._score;
    }

    public get tags(): Array<string>
    {
        return this._tags;
    }

    public get title(): string
    {
        return this._title;
    }

    public get view_count(): number
    {
        return this._view_count;
    }

    //endregion

    public Equals(other: SearchResultItem): boolean
    {
        return super.Equals(other) &&
               this._answer_count == other._answer_count &&
               this._closed_date == other._closed_date &&
               this._closed_reason == other._closed_reason &&
               this._creation_date == other._creation_date &&
               this._is_answered == other._is_answered &&
               this._last_activity_date == other._last_activity_date &&
               this._link == other._link &&
               this._score == other._score &&
               this._tags.length == other._tags.length &&
               this._tags.every((item: string, index: number) =>
               {
                   return item == other._tags[index];
               }) &&
               this._title == other._title &&
               this._view_count == other._view_count;
    }
}
