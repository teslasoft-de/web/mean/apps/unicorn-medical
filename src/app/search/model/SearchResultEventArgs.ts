import {EventArgs} from 'base/model';
import {SearchResultItemBase} from 'search/model';


/**
 * Represents an event args object holding search result items.
 */
export class SearchResultEventArgs<T extends SearchResultItemBase = T> extends EventArgs
{
    public static get Empty(): SearchResultEventArgs<any>
    {
        return <SearchResultEventArgs>EventArgs.Empty;
    }

    public get SearchName(): string
    {
        return this._SearchName;
    }

    public get SearchResultItems(): Array<T>
    {
        return this._SearchResultItems;
    }

    constructor(private readonly _SearchName: string, private readonly _SearchResultItems?: Array<T>)
    {
        super();
    }

    /**
     * Checks equality by comparing the SearchResultItems array member.
     * @inheritDoc */
    public Equals(other: SearchResultEventArgs): boolean
    {
        return super.Equals(other) &&
               this._SearchName == other._SearchName &&
               ((!this._SearchResultItems && !other._SearchResultItems) ||
                this._SearchResultItems && other._SearchResultItems && (
                    this._SearchResultItems.length == other._SearchResultItems.length &&
                    this._SearchResultItems.every((item: T, index: number) =>
                    {
                        return item.Equals(other._SearchResultItems[index]);
                    })));
    }
}
