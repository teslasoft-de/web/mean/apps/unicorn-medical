import {Disposable} from 'base/model';
import {environment} from 'environments/environment';
import {StackOverflowSearch} from 'search/model/StackOverflowSearch';
import {WeatherSearch} from 'search/model/WeatherSearch';


export class Search extends Disposable
{
    private readonly _StackOverflowSearches = environment.searches.map(search => new StackOverflowSearch(search));
    private readonly _WeatherSearch         = new WeatherSearch();

    public get StackOverflowSearches(): Array<StackOverflowSearch>
    {
        return this._StackOverflowSearches;
    }

    public get WeatherSearch(): WeatherSearch
    {
        return this._WeatherSearch;
    }

    constructor()
    {
        super();
    }

    /**@inheritDoc*/
    public Dispose(disposing?: boolean)
    {
        if (disposing && !this.IsDisposed) {
            this._StackOverflowSearches.forEach(search => search.Dispose());
        }
        super.Dispose(disposing);
    }
}
