import {ActionObserver} from 'base/model';
import {Observable} from 'rxjs';
import {SearchResultEventArgs} from 'search/model/SearchResultEventArgs';
import {IWeatherSearchService, WeatherSearchResultItem} from 'search/model/WeatherSearch';
import {ServiceBase} from 'search/services';


export class WeatherSearch<T extends WeatherSearchResultItem = T> extends ActionObserver<Array<T>>
{
    public get Heading(): string
    {
        let heading = 'Weather';

        if (this.NextValue.length) {
            heading += ` (${this.NextValue.length})`;
        }

        return heading;
    }

    constructor()
    {
        super(null, []);

        this.OnError = ServiceBase.HandleError;
    }

    public async Search(searchService: IWeatherSearchService, city: string): Promise<Observable<Array<T>>>
    {
        this.Reset();

        return await this.Observe(
            () => <Observable<Array<T>>>searchService.Search(city),
            {
                init    : () => new SearchResultEventArgs(city),
                next    : (searchResult) => new SearchResultEventArgs(city, searchResult),
                complete: () => new SearchResultEventArgs(city, this.ActionResultValue)
            }
        );
    }
}
