import {SearchResultItemBase} from 'search/model/SearchResultItemBase';


interface DataStore
{
    [prop: string]: string | boolean | number
}

export class WeatherSearchResultItem extends SearchResultItemBase
{
    constructor(
        /**
         * @format DD.MM.YY
         */
        public readonly Date: string,
        /**
         * @format mm:mm
         */
        public readonly Time: string,
        /**
         * @unit °C
         */
        public readonly TempMin: number,
        /**
         * @unit °C
         */
        public readonly TempMax: number,
        /**
         * @unit %
         */
        public readonly Moisture: number,
        /**
         * @unit hPa
         */
        public readonly Pressure: number,
        /**
         * @unit mm
         */
        public readonly Rain: number,
        /**
         * km/h
         */
        public readonly Wind: number,
        /**
         * °
         */
        public readonly Direction: number,
        /**
         *
         */
        public readonly Brightness: number)
    {
        super();
    }

    public static FromJSON(data: DataStore)
    {
        return new WeatherSearchResultItem(
            <string>data['Datum'],
            <string>data['Zeit'],
            <number>data['Temp. A.'],
            <number>data['Temp. 3'],
            <number>data['Feuchte A.'],
            <number>data['Luftdruck'],
            <number>data['Regen'],
            <number>data['Wind'],
            <number>data['Richtung'],
            <number>data['Helligkeit']
        );
    }
}
