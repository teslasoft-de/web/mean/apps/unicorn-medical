import {IService} from 'base/services';
import {Observable} from 'rxjs';
import {WeatherSearchResultItem} from 'search/model/WeatherSearch';


export interface IWeatherSearchService extends IService
{
    IsSearchInProgress: boolean;
    Search: WeatherSearchRequest;
}

export interface WeatherSearchRequest
{
    (keyword: string): Observable<Array<WeatherSearchResultItem>>
}
