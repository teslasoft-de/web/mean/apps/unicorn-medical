import {Component} from '@angular/core';

import {AppViewModel} from 'app/core/app/view-model/AppViewModel';
import {ComponentBase} from 'base/view';
import {SlideInAnimation} from 'core/routing/view';


@Component({
    selector   : 'app-root',
    templateUrl: './AppComponent.html',
    styleUrls  : ['./AppComponent.scss'],
    providers  : [AppViewModel],
    animations : [SlideInAnimation]
})
export class AppComponent extends ComponentBase<AppViewModel>
{
    constructor(vm: AppViewModel)
    {
        super(vm);
    }
}
