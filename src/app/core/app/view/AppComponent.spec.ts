import {APP_BASE_HREF} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {async, TestBed} from '@angular/core/testing';

import {AppComponent} from 'app';
import {LayoutModule} from 'core/layout';
import {AppRoutingModule} from 'core/routing';
import {DashboardComponent, LatestSearchComponent} from 'dashboard/view';
import {SearchComponent, StackOverflowSearchComponent} from 'search/view';


describe('AppComponent', () =>
{
    beforeEach(async(() =>
    {
        TestBed.configureTestingModule({
            imports     : [
                LayoutModule,
                AppRoutingModule,
                HttpClientModule
            ],
            declarations: [
                AppComponent,
                DashboardComponent,
                LatestSearchComponent,
                SearchComponent,
                StackOverflowSearchComponent
            ],
            providers   : [
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        }).compileComponents();
    }));

    it('should create the app', async(() =>
    {
        const fixture = TestBed.createComponent(AppComponent);
        const app     = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
