import {Injectable} from '@angular/core';

import {ViewModelBase} from 'base/view-model';
import {App} from 'core/app/model/App';


@Injectable()
export class AppViewModel extends ViewModelBase<App>
{
    public get Model(): App
    {
        return App.Instance;
    }

    constructor()
    {
        super();
    }
}
