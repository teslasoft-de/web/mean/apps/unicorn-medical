import {LatestSearchResult} from 'dashboard/model/LastestSearchResult';
import {BehaviorSubject, Observable} from 'rxjs';
import {SearchResultItem} from 'app/search/model/StackOverflowSearch';


export class SearchResult<T extends SearchResultItem = T>
{
    private readonly _Result  = new BehaviorSubject<Map<string, Array<LatestSearchResult<T>>>>(new Map());
    private readonly _Result$ = this._Result.asObservable();

    public get Map(): Observable<Map<string, Array<LatestSearchResult<T>>>>
    {
        return this._Result$;
    }

    public Add(key: string, value: Array<T>): void
    {
        const map    = this._Result.getValue();
        let searches = map.get(key);
        if (!searches) {
            map.set(key, searches = []);
        }
        searches.push(new LatestSearchResult<T>(key, value));
        this._Result.next(map);
    }
}
