import {LatestSearchResult} from 'app/dashboard/model';
import {ObjectModel} from 'base/model';
import {SearchResult} from 'core/app/model/SearchResult';
import {environment} from 'environments/environment';
import {Observable} from 'rxjs';
import {ISearchService, SearchResultItem} from 'search/model/StackOverflowSearch';


/**
 * Represents the application master state.
 */
export class App extends ObjectModel
{
    //region App Singleton (Application Master State)

    private static _Instance: App;

    public static get Instance(): App
    {
        return this._Instance || (this._Instance = new this());
    }

    //endregion

    private readonly _Name: string;
    private readonly _LatestSearches = new SearchResult();

    //region Public Properties

    public get Name(): string
    {
        return this._Name;
    }

    /**
     * Returns the latest searches provided through the `App.SearchTopics` operation.
     */
    public get LatestSearches(): Observable<Map<string, Array<LatestSearchResult>>>
    {
        return this._LatestSearches.Map;
    }

    //endregion

    constructor()
    {
        super();

        let appName = 'Unicorn Medical';
        if (!environment.production) {
            appName += ` (Development Mode)`;
        }
        document.title = appName;
        this._Name     = appName;
    }

    /**
     * Performs the search for specific search service topics.
     * The search service can by any kind of search because the object model only knows the `ISearchService` interface.
     * @param searchService
     * @param keyword
     */
    public SearchTopics<T extends SearchResultItem = T>(searchService: ISearchService, keyword: string): Observable<Array<T>>
    {
        const observable = searchService.Search(keyword);
        let result: Array<T>;
        observable.subscribe({
            next    : (res: Array<T>) => {result = res;},
            complete: () =>
            {
                console.log(`'${keyword}' search result contained '${result.length}' entries.`);
                this._LatestSearches.Add(keyword, result);
            }
        });
        return <Observable<Array<T>>>observable;
    }
}
