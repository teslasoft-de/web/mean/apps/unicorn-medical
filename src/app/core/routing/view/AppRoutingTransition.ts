import {animate, group, query, style, transition, trigger} from '@angular/animations';


const transitionStart = query(
    ':enter, :leave',
    style({position: 'fixed', width: '100%'}),
    {optional: true}
);

export function slide(axis: 'X' | 'Y', from: string, to: string, duration = '0.5', animation = `ease-in-out`)
{
    return group([
        query(':enter', [
            style({transform: `translate${axis}(${from})`}),
            animate(
                `${duration}s ${animation}`,
                style({transform: 'translateX(0%)'})
            )
        ], {optional: true}),
        query(':leave', [
            style({transform: 'translateX(0%)'}),
            animate(
                `${duration}s ${animation}`,
                style({transform: `translateX(${to})`})
            )
        ], {optional: true})
    ]);
}

export const transitionToLeft  = slide('X', '100%', '-100%');
export const transitionToRight = slide('X', '-100%', '100%');

export const SlideInAnimation = trigger('routeAnimations', [
    transition('dashboard => search', [transitionStart, transitionToRight]),
    transition('search => dashboard', [transitionStart, transitionToLeft]),
    transition('* => *', [transitionStart, transitionToLeft])
]);
