import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from 'app/dashboard/view';
import {SearchComponent} from 'app/search/view';


export const AppRoutes: Routes = [
    {
        path: 'dashboard', component: DashboardComponent,
        data: {animation: 'dashboard'}
    },
    {
        path: 'search', component: SearchComponent,
        data: {animation: 'search'}
    },
    {
        path: '**', component: DashboardComponent,
        data: {animation: 'void'}
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
