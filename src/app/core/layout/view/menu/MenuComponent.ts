import {Component} from '@angular/core';
import {ComponentBase} from 'base/view';
import {MenuViewModel} from 'core/layout/view-model';


@Component({
    selector   : 'ae-main-navigation',
    templateUrl: './MenuComponent.html',
    styleUrls  : ['MenuComponent.scss'],
    providers  : [MenuViewModel]
})
export class MenuComponent extends ComponentBase<MenuViewModel>
{
    constructor(vm: MenuViewModel)
    {
        super(vm);
    }
}
