import {Component} from '@angular/core';

import {ComponentBase} from 'base/view';
import {NavbarViewModel} from 'core/layout/view-model';


@Component({
    selector   : 'ae-navbar',
    templateUrl: './NavbarComponent.html',
    styleUrls  : ['./NavbarComponent.css'],
    providers  : [NavbarViewModel]
})
export class NavbarComponent extends ComponentBase<NavbarViewModel>
{
    constructor(vm: NavbarViewModel)
    {
        super(vm);
    }
}
