import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from 'app/core/routing';
import {MenuComponent, NavbarComponent} from 'core/layout/view';


@NgModule({
    imports     : [
        AppRoutingModule,
        BrowserModule
    ],
    declarations: [
        NavbarComponent,
        MenuComponent
    ],
    exports     : [
        NavbarComponent,
        MenuComponent
    ]
})
export class LayoutModule
{}
