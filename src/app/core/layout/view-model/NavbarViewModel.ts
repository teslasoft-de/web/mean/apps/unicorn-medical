import {App} from 'core/app/model/App';
import {ViewModelBase} from 'base/view-model';


export class NavbarViewModel extends ViewModelBase<App>
{
    public get Name(): string
    {
        return 'Navbar Komponente...';
    }

    public get Model(): App
    {
        return App.Instance;
    }
}
