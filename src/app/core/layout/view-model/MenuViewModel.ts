import {ViewModelBase} from 'base/view-model';
import {IMenuActionItem, IMenuItem, Menu} from 'core/layout/model';


export class MenuViewModel extends ViewModelBase<Menu>
{
    //region Public Properties

    public get Model(): Menu
    {
        return Menu.Instance;
    }

    public get ActionItems(): Array<IMenuActionItem>
    {
        return this.Model.ActionItems;
    }

    public get MenuItems(): Array<IMenuItem>
    {
        return this.Model.Items;
    }

    //endregion

    constructor()
    {
        super();
    }
}
