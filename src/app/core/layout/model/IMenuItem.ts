interface IAbstractMenuItem
{
    /**
     * Title of the menu items. Used as name in menu
     */
    Title: string;
}

export interface IMenuItem extends IAbstractMenuItem
{
    /**
     * Font-Awesome icon (without 'fa-')
     * Displayed left to the title
     */
    Icon: string;

    /**
     * State to be opened when the user clicks on the menu entry
     * Neccessary if a menu item has no children (item attribute) and createModalModelName is not defined.
     */
    State: string;
}

export interface IMenuActionItem extends IAbstractMenuItem
{
    Link: string;
    Icon1: string;
    Icon2: string;
}
