import {ObjectModel} from 'base/model';
import {IMenuActionItem, IMenuItem} from 'core/layout/model/IMenuItem';


export class Menu extends ObjectModel
{
    //region Singleton Access

    private static _Instance: Menu;

    public static get Instance(): Menu
    {
        return this._Instance || (this._Instance = new this());
    }

    //endregion

    private _Items: Array<IMenuItem> = [{
        Title: 'Dashboard',
        Icon : 'home',
        State: 'dashboard'
    }, {
        Title: 'Search',
        Icon : 'search',
        State: 'search'
    }];

    private _ActionItems: Array<IMenuActionItem> = [{
        Link : 'https://wiki.redmedical.de/x/gIE4',
        Icon1: 'square',
        Icon2: 'phone',
        Title: 'Hilfe'
    }];

    //region Public Properties

    public get Items(): IMenuItem[]
    {
        return this._Items;
    }

    public get ActionItems(): IMenuActionItem[]
    {
        return this._ActionItems;
    }

    //endregion
}
