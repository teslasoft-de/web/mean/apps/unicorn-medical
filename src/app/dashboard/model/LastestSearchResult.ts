import {ObjectModel} from 'base/model';
import {SearchResultItem} from 'search/model/StackOverflowSearch';


export class LatestSearchResult<T extends SearchResultItem = T> extends ObjectModel
{
    private readonly _Topic: string;
    private readonly _Results: Array<T>;

    constructor(SearchTopic: string, SearchResults: Array<T>)
    {
        super();
        this._Topic   = SearchTopic;
        this._Results = SearchResults;
    }

    public get Topic(): string
    {
        return this._Topic;
    }

    public get Results(): Array<T>
    {
        return this._Results;
    }

    public get MostPopularTopic()
    {
        return this._Results.reduce((previous: T, current: T) =>
        {
            return previous.view_count > current.view_count ? previous : current;
        }).title;
    }

    public get TotalAnswers()
    {
        return this._Results.reduce((sum, current) => sum + current.answer_count, 0);
    }

    public get TotalAnswered()
    {
        return this._Results.reduce((sum, current) => sum + Number(current.is_answered), 0);
    }

    public get AnswerStatistic()
    {
        return this.TotalAnswered / this.Results.length * 100;
    }

    public Equals(other: LatestSearchResult): boolean
    {
        return super.Equals(other) &&
               this._Topic == other._Topic &&
               this._Results.length == other._Results.length &&
               this._Results.every((item: T, index: number) =>
               {
                   return item.Equals(other._Results[index]);
               });
    }
}
