import {Disposable} from 'base/model';
import {LatestSearch} from 'dashboard/model/LatestSearch';
import {environment} from 'environments/environment';


export class Dashboard extends Disposable
{
    private _LatestSearches = environment.searches.map(search => new LatestSearch(search));

    public get LatestSearches(): Array<LatestSearch>
    {
        return this._LatestSearches.filter(search => !!search.ResultsAvailable) || [];
    }

    /**@inheritDoc*/
    public Dispose(disposing?: boolean)
    {
        if (disposing && !this.IsDisposed) {
            this._LatestSearches.forEach(search => search.Dispose());
        }
        super.Dispose(disposing);
    }
}
