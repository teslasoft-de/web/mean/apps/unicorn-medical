import {ActionObserver, Disposable} from 'base/model';
import {App} from 'app';
import {LatestSearchResult} from 'dashboard/model/LastestSearchResult';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {ISearchService, SearchResultItem} from 'app/search/model/StackOverflowSearch';
import {ServiceBase} from 'app/search/services';


export class LatestSearch<T extends SearchResultItem = T> extends Disposable
{
    private readonly _RerunSearchObserver: ActionObserver<Array<T>>;
    private readonly _Results  = new BehaviorSubject<Array<LatestSearchResult<T>>>([]);
    private readonly _Results$ = this._Results.asObservable();
    private _LatestSearchesSubscription: Subscription;

    public get Topic(): string
    {
        return this._Topic;
    }

    public get Results(): Observable<Array<LatestSearchResult<T>>>
    {
        return this._Results$;
    }

    public get ResultsAvailable(): boolean
    {
        return !!this._Results.getValue();
    }

    constructor(private readonly _Topic: string)
    {
        super();

        this._RerunSearchObserver         = new ActionObserver(this);
        this._RerunSearchObserver.OnError = ServiceBase.HandleError;

        this._LatestSearchesSubscription = App.Instance.LatestSearches.subscribe((value: Map<string, Array<LatestSearchResult<T>>>) =>
        {
            this._Results.next(value.get(this._Topic));
        });
    }

    public async RerunSearch(searchService: ISearchService, topic: string)
    {
        return await this._RerunSearchObserver.Observe(() => App.Instance.SearchTopics(searchService, topic));
    }

    public Equals(other: LatestSearch<T>): boolean
    {
        return super.Equals(other) &&
               this._Topic == other._Topic &&
               this.Results == other.Results;
    }

    /**@inheritDoc*/
    public Dispose(disposing?: boolean)
    {
        if (disposing && !this.IsDisposed) {
            this._RerunSearchObserver.Dispose();
            this._LatestSearchesSubscription.unsubscribe();
        }
        super.Dispose(disposing);
    }
}
