import {async} from '@angular/core/testing';

import {App, AppModule} from 'app';
import {LatestSearch} from 'dashboard/model';
import {LatestSearchViewModel} from 'dashboard/view-model';
import {LatestSearchComponent} from 'dashboard/view/';
import {SearchServiceMock} from 'search/services/StackOverflowSearch';
import {Shallow} from 'shallow-render';


describe('LatestSearchComponent', () =>
{
    let shallow: Shallow<LatestSearchComponent>;

    beforeEach(async(async () =>
    {
        shallow = new Shallow(LatestSearchComponent, AppModule);
        shallow.dontMock(LatestSearchViewModel);
        await App.Instance.SearchTopics(new SearchServiceMock(), 'topic').toPromise();
    }));

    it('should create', async () =>
    {
        const {outputs} = await shallow.render({
            bind: {
                Model: new LatestSearch('topic')
            }
        });
        expect(outputs).toBeTruthy();
    });
});
