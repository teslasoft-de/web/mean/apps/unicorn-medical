import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';

import {ComponentBase} from 'base/view';
import {LatestSearchViewModel} from 'dashboard/view-model';
import {CarouselComponent} from 'ngx-bootstrap';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';


@Component({
    selector       : 'app-latest-search',
    templateUrl    : './LatestSearchComponent.html',
    styleUrls      : ['./LatestSearchComponent.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers      : [LatestSearchViewModel]
})
export class LatestSearchComponent extends ComponentBase<LatestSearchViewModel>
{
    private readonly _ProgressChars = ['-', '\\', '|', '/'];
    private _ProgressIndicator      = new BehaviorSubject<string>(null);
    private _ProgressIndicator$     = this._ProgressIndicator.asObservable();
    private _ProgressStep           = 0;

    @ViewChild(CarouselComponent)
    private CarouselComponent!: CarouselComponent;
    private _RerunSearchSubscription: Subscription;

    public get ProgressIndicator(): Observable<string>
    {
        return this._ProgressIndicator$;
    }

    constructor(vm: LatestSearchViewModel, private _ChangeDetector: ChangeDetectorRef)
    {
        super(vm);
    }

    private AnimateProgress()
    {
        this._ProgressIndicator.next(`(${this._ProgressChars[this._ProgressStep++]})`);
        if (this._ProgressStep == this._ProgressChars.length) {
            this._ProgressStep = 0;
        }
    }

    public SearchDone()
    {
        this._ProgressIndicator.next(null);

        // Fix for updating the carousel component with new amount of slides.
        // TODO: Post bug report on github (https://github.com/valor-software/ngx-bootstrap)
        (<any>this.CarouselComponent).ngAfterViewInit();
        setTimeout(() =>
        {
            const index = this.CarouselComponent.slides.length - 1;
            this.CarouselComponent.slides.forEach(slide => slide.active = false);
            this.CarouselComponent.selectSlide(index);
            this._ChangeDetector.detectChanges();
        }, 1);
    };

    public RerunSearchAction()
    {
        this.vm.RerunSearchAction({
            sender: this.vm,
            topic : this.vm.Model.Topic
        }).then(observable =>
        {
            this._RerunSearchSubscription = observable.subscribe({
                next    : this.AnimateProgress.bind(this),
                complete: this.SearchDone.bind(this)
            });
        });
    }

    public ngOnDestroy(): void
    {
        this._RerunSearchSubscription && this._RerunSearchSubscription.unsubscribe();
        super.ngOnDestroy();
    }
}
