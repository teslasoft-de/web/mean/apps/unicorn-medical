import {Component} from '@angular/core';

import {ComponentBase} from 'base/view';
import {DashboardViewModel} from 'dashboard/view-model/DashboardViewModel';


@Component({
    selector   : 'app-dashboard',
    templateUrl: './DashboardComponent.html',
    styleUrls  : ['./DashboardComponent.scss'],
    providers  : [DashboardViewModel]
})
export class DashboardComponent extends ComponentBase<DashboardViewModel>
{
    public get ColumnSpacing(): string
    {
        const length = this.vm.Model.LatestSearches.length;
        return `col-lg-${12 / (length < 4 ? length : (length == 4 ? 2 : 3))}`;
    }

    constructor(vm: DashboardViewModel)
    {
        super(vm);
    }
}
