import {async} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

import {App, AppModule} from 'app';
import {DashboardComponent, LatestSearchComponent} from 'dashboard/view';
import {DashboardViewModel} from 'dashboard/view-model';
import {SearchServiceMock} from 'search/services/StackOverflowSearch';
import {Shallow} from 'shallow-render';


describe('DashboardComponent', () =>
{
    let shallow: Shallow<DashboardComponent>;

    beforeEach(async(async () =>
    {
        shallow = new Shallow(DashboardComponent, AppModule);
        shallow.dontMock(
            DashboardViewModel
        );
        await App.Instance.SearchTopics(new SearchServiceMock(), 'topic').toPromise();
    }));

    it('should create', async () =>
    {
        const {outputs} = await shallow.render();
        expect(outputs).toBeTruthy();
    });

    it('should display latest searches', async () =>
    {
        const {fixture} = await shallow.render();
        expect(fixture.debugElement.query(By.directive(LatestSearchComponent))).toBeTruthy();
    });
});
