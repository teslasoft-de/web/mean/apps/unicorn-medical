import {Command, CommandParameters, IViewModel, ViewModelBase} from 'base/view-model';
import {LatestSearch} from 'app/dashboard/model/LatestSearch';
import {SearchResultItem} from 'search/model/StackOverflowSearch';
import {SearchService} from 'search/services/StackOverflowSearch';


export interface ILatestSearchViewModel<T extends SearchResultItem = T> extends IViewModel<LatestSearch<T>>
{
    Name: string;
    IsSearchInProgress: boolean;
    RerunSearchAction: Command<RerunSearchBindingParameters<T>, Array<T>>
}

export interface RerunSearchBindingParameters<T extends SearchResultItem = T> extends CommandParameters
{
    topic: string;
}

export class LatestSearchViewModel<T extends SearchResultItem = T>
    extends ViewModelBase<LatestSearch<T>> implements ILatestSearchViewModel
{
    public get Name(): string
    {
        return 'LatestSearch Komponente...';
    }

    public get IsSearchInProgress(): boolean
    {
        return this._SearchService.BackgroundWorkers.has(this.Model.Topic);
    }

    constructor(private _SearchService: SearchService)
    {
        super();
    }

    public RerunSearchAction = async (params: RerunSearchBindingParameters<T>) =>
    {
        return await this.Model.RerunSearch(this._SearchService, params.topic);
    };
}
