import {IViewModel, ViewModelBase} from 'base/view-model';
import {Dashboard} from 'dashboard/model/Dashboard';


export interface IDashboardViewModel extends IViewModel<Dashboard>
{}

export class DashboardViewModel extends ViewModelBase<Dashboard> implements IDashboardViewModel
{
    private _Dashboard = new Dashboard();

    public get Name(): string
    {
        return 'Dashboard Komponente...';
    }

    public get Model(): Dashboard
    {
        return this._Dashboard;
    }

    public Destroy(): void
    {
        this._Dashboard.Dispose();
    }
}
