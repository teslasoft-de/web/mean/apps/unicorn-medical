import {SimpleChanges} from '@angular/core';
import {Disposable, IDisposable, ObjectModel} from 'base/model';
import {IViewModel} from 'base/view-model';


/**
 * Represents the base for all view models in the model-view-view-model pattern.
 */
export abstract class ViewModelBase<TModel extends ObjectModel> extends ObjectModel implements IViewModel<TModel>
{
    private _Model: TModel;

    public get Name(): string
    {
        return this.toString();
    }

    /**
     * Returns the model instance which is associated with this view-model instance.
     */
    public get Model(): TModel
    {
        return this._Model;
    }

    /**
     * Returns the model instance which is associated with this view-model instance.
     */
    public set Model(value: TModel)
    {
        this._Model = value;
    }

    /** @inheritDoc */
    public Init?(): void

    /** @inheritDoc */
    public ViewInit?(): void

    /** @inheritDoc */
    public ViewChecked?(): void;

    /** @inheritDoc */
    public ContentInit?(): void;

    /** @inheritDoc */
    public ContentChecked?(): void;

    /** @inheritDoc */
    public Changes?(changes: SimpleChanges): void;

    /** @inheritDoc */
    public Destroy(): void
    {
        this.Model && (this.Model instanceof Disposable || (<IDisposable><any>this.Model).Dispose) && (<IDisposable><any>this.Model).Dispose();
    }

    /**
     * Checks equality by comparing the name property of this instance.
     * @inheritDoc
     * @param other
     */
    public Equals(other: ViewModelBase<TModel>): boolean
    {
        return super.Equals(other) &&
               this.Name == other.Name;
    }
}
