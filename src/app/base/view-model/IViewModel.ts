import {SimpleChanges} from '@angular/core';
import {ObjectModel} from 'base/model';


/**
 * Defines the event lifecycle hook signature for every view-model object.
 */
export interface IViewModel<T extends ObjectModel = T> extends ObjectModel
{
    Name: string;
    Model: T;

    /**
     * A lifecycle hook that is called after Angular has initialized
     * all data-bound properties of a directive.
     */
    Init?(): void;

    /**
     * A lifecycle hook that is called after Angular has fully initialized a component's view.
     */
    ViewInit?(): void;

    /**
     * A lifecycle hook that is called after the default change detector has completed checking a component's view for changes.
     */
    ViewChecked?(): void;

    /**
     * A lifecycle hook that is called after Angular has fully initialized all content of a directive.
     * When implemented in a derived class, it can be used to handle any additional initialization tasks.
     */
    ContentInit?(): void;

    /**
     * A lifecycle hook that is called after the default change detector has completed checking all content of a directive.
     */
    ContentChecked?(): void;

    /**
     * A lifecycle hook that is called when any data-bound property of a directive changes.
     * @param changes The changed properties.
     */
    Changes?(changes: SimpleChanges): void;

    /**
     * A lifecycle hook that is called when a directive, pipe, or service is destroyed.
     * Use for any custom cleanup that needs to occur when the instance is destroyed.
     */
    Destroy?(): void;
}
