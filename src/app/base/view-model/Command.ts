import {EventArgs} from 'base/model';
import {IViewModel} from 'base/view-model';
import {Observable} from 'rxjs';


/**
 * Command binding method signature for View-Model Command invocations using the command pattern.
 * Use this signature to declare commands within view-models providing a configurable parameter layout for the event handler delegate.
 * Implement invocations from within view-models or web framework component code only and never directly from within the view.
 * @example
 * TODO
 */
export interface Command<TParams extends CommandParameters = TParams, TResult = void>
{
    /**
     *
     * @param params Acts as the parameter store for the event handler invocation arguments.
     * @returns Can return a `Promise` which itself can return a `Observable` used to append presentation and workflow logic to the command
     *     action.
     */
    (params: TParams): void | Promise<void | Observable<void | TResult>>
}

/**
 * Command binding parameters base signature with event sender definition.
 */
export interface CommandParameters<T extends IViewModel = T>
{
    sender: T
}

/**
 * Command binding parameters signature with event arguments definition.
 */
export interface CommandHandler<TArgs extends EventArgs = TArgs, TViewModel extends IViewModel = TViewModel>
    extends CommandParameters<TViewModel>
{
    eventArgs: TArgs
}
