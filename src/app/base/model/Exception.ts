/**
 * Represents the base class for every exception thrown from within the object model.
 * Derive from this class to create exception domains to detect exception origin locations.
 */

export class Exception extends Error
{
    /**
     * Sets the prototype explicitly for the calling type.
     * Call this method always from within derived instances to set the exception prototype to this type.
     * @param instance
     * @constructor
     */
    protected static SetPrototype(instance: Exception)
    {
        Object.setPrototypeOf(instance, this.prototype);
    }

    /**
     * Determines if the provided error instance is typeof this instance.
     * Works only if `SetPrototype` has been called from with the constructor of derived exception types.
     * @param exceptionDomain
     */
    public static TypeOf(exceptionDomain: Error)
    {
        return this == exceptionDomain.constructor;
    }

    public get Message()
    {
        return this.message;
    }

    public get StackTrace()
    {
        return this.stack;
    }

    constructor(message: string)
    {
        super(message);

        Exception.SetPrototype(this);
    }

    public toString()
    {
        return this.message;
    }
}
