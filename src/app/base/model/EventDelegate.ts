import {EventEmitter} from '@angular/core';

import {EventArgs, IEvent, IEventDelegate} from 'base/model/EventArgs';
import {ObjectModel} from 'base/model/ObjectModel';
import {Subscription} from 'rxjs';


export type EventObserver<TSender extends ObjectModel,
    TEventArgs extends EventArgs = TEventArgs> = {
    next: IEvent<TSender, TEventArgs>;
    error?: (err: any) => void;
    complete?: () => void;
}

/**
 * Represents a simple event delegate for encapsulating the angular EventEmitter object and delegates access to the rxjs api only.
 */
export class EventDelegate<TSender extends ObjectModel, TEventArgs extends EventArgs = TEventArgs>
{
    private readonly _EventEmitter: EventEmitter<IEventDelegate<TSender, TEventArgs>>;
    private readonly _Subscriptions = new Map<IEvent<TSender, TEventArgs>, Subscription>();

    /**
     * Returns the angular EventEmitter used only within angular components.
     * @access Any access from a view-model, model or service is not permitted because it couples angular code to the object model.
     */
    public get EventEmitter(): EventEmitter<IEventDelegate<TSender, TEventArgs>>
    {
        return this._EventEmitter;
    }

    public get IsAsync(): boolean
    {
        return this._EventEmitter.__isAsync;
    }

    constructor(protected _Producer: TSender, isAsync?: boolean)
    {
        this._EventEmitter = new EventEmitter(isAsync);
    }

    /**
     * Subscribes to the underlying EventEmitter observable and delegates event subscription to commend pattern style handler functions.
     * @param observer The next observer function or an event observer instance.
     * @param thisArg If provided, all observer functions are invoked on except the error observer.
     */
    public Subscribe(
        observer: IEvent<TSender, TEventArgs> | EventObserver<TSender, TEventArgs>,
        thisArg?: object): Subscription
    {
        let next: IEvent<TSender, TEventArgs>,
            error: (err: any) => void,
            complete: () => void;

        if (typeof observer == 'function') {
            next = observer;
        } else {
            ({next, error, complete} = observer);
        }

        if (this._Subscriptions.get(next)) {
            this.Unsubscribe(next);
        }

        const subscription = this._EventEmitter.subscribe(
            (delegate: IEventDelegate<TSender, TEventArgs>) =>
            {
                delegate(thisArg ? next.bind(thisArg) : next);
            },
            error && thisArg ? error.bind(thisArg) : error,
            complete && thisArg ? complete.bind(thisArg) : complete
        );
        subscription.add(() =>
        {
            this._Subscriptions.delete(next);
        });
        this._Subscriptions.set(next, subscription);
        return subscription;
    }

    public Unsubscribe(observer: IEvent<TSender, TEventArgs> | EventObserver<TSender, TEventArgs>): void
    {
        const subscription = this._Subscriptions.get(typeof observer == 'function' ? observer : observer.next);
        if (subscription.closed === false) {
            subscription.unsubscribe();
        }
    }

    public Invoke(ea: TEventArgs = <TEventArgs>EventArgs.Empty): void
    {
        this._EventEmitter.emit((delagate: IEvent<TSender, TEventArgs>, thisArg: object) =>
        {
            (thisArg ? delagate.bind(thisArg) : delagate)(this._Producer, ea);
        });
    }
}
