import {ObjectModel} from 'base/model/ObjectModel';


/**
 * Represents a event data container used within the object event model,
 * and provides a value to use for events that do not include event data.
 */
export class EventArgs extends ObjectModel
{
    static get Empty(): EventArgs
    {
        return this._Empty;
    }

    private static _Empty: EventArgs = new EventArgs();
}

/**
 * Defines a delegate signature used to invoke command handler delegates on raised events.
 */
export interface IEventDelegate<TSender extends ObjectModel, TEventArgs extends EventArgs = TEventArgs>
{
    (delegate: IEvent<TSender, TEventArgs>, thisArg?: object): void
}

/**
 * Defines the delegate signature for the event model using the command pattern.
 */
export interface IEvent<TSender extends ObjectModel, TEventArgs extends EventArgs = TEventArgs>
{
    (sender: TSender, eventArgs?: TEventArgs): void | Promise<void>
}
