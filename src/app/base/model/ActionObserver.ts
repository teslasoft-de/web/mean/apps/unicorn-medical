import {Disposable} from 'base/model/Disposable';
import {EventArgs, IEvent} from 'base/model/EventArgs';
import {ObjectModel} from 'base/model/ObjectModel';
import {BehaviorSubject, Observable, PartialObserver, Subscription, throwError} from 'rxjs';


export type PartialActionObserver<T, TEventArgs extends EventArgs = EventArgs> = {
    init?: () => TEventArgs;
    next?: (value: T) => TEventArgs;
    error?: (error: any) => void,
    complete?: () => TEventArgs;
}

/**
 * A object model utility for watching observables using command pattern style handler functions
 * which are bound to the provided {@see ActionObserver._ThisArg} argument.
 */
export class ActionObserver<T> extends Disposable
{
    //<editor-fold desc="Instance Members">

    private readonly _ThisArg: ObjectModel;
    private _ActionObservable: Observable<T>;
    private _ActionSubscription: Subscription;
    private readonly _NextResult: BehaviorSubject<T>;
    private readonly _NextResult$: Observable<T>;
    private readonly _ActionResult: BehaviorSubject<T>;
    private readonly _ActionResult$: Observable<T>;
    private _OnProgress: IEvent<ObjectModel>;
    private _OnError: (e: Error) => void;
    private _OnComplete: IEvent<ObjectModel>;

    //</editor-fold>

    //<editor-fold desc="Public Properties">

    //<editor-fold desc="Action Observable">

    /**
     * Returns the observable returned from the last observed action.
     * @description Subscribe to observe the latest observed action.
     */
    public get Action(): Observable<T>
    {
        return this._ActionObservable;
    }

    /**
     * Returns the last 'next' result from the last observed action.
     * @description Subscribe to receive the final 'next' result of any called action.
     */
    public get ActionResult(): Observable<T>
    {
        return this._ActionResult$;
    }

    public get ActionResultValue(): T
    {
        return this._ActionResult.getValue();
    }

    /**
     * Returns a boolean value indicating weather this instance has ever received a result
     * other than the `ActionObserver._DefaultResult` from an action.
     */
    public get ActionResultAvailable(): boolean
    {
        return !!this.ActionResultValue && this.ActionResultValue !== this._DefaultResult;
    }

    //</editor-fold>

    //<editor-fold desc="Next Result Observable">

    /**
     * Returns the 'next' result from the currently observed action.
     * @description Subscribe to receive the 'next' result of any called action.
     */
    public get NextResult(): Observable<T>
    {
        return this._NextResult$;
    }

    public get NextValue(): T
    {
        return this._NextResult.getValue();
    }

    /**
     * Returns a boolean value indicating weather this instance has ever received a result
     * other than the `ActionObserver._DefaultResult` from an action.
     */
    public get NextResultAvailable(): boolean
    {
        return !!this.NextValue && this.NextValue !== this._DefaultResult;
    }

    //</editor-fold>

    public set OnProgress(value: IEvent<ObjectModel>)
    {
        this._OnProgress = value.bind(this._ThisArg);
    }

    public set OnError(value: (e: Error) => void)
    {
        this._OnError = value;
    }

    public set OnComplete(value: IEvent<ObjectModel>)
    {
        this._OnComplete = value.bind(this._ThisArg);
    }

    //</editor-fold>

    /**
     *
     * @param _ThisArg If provided, all observer functions are invoked on except the error observer.
     * @param _DefaultResult if provided, it will be used as default value for the
     */
    constructor(
        _ThisArg?: ObjectModel, private readonly _DefaultResult?: T)
    {
        super();
        this._ThisArg       = _ThisArg || this;
        this._NextResult    = new BehaviorSubject<T>(_DefaultResult);
        this._NextResult$   = this._NextResult.asObservable();
        this._ActionResult  = new BehaviorSubject(_DefaultResult);
        this._ActionResult$ = this._ActionResult.asObservable();
    }

    /**
     * Observes actions by additionally using a provided next observer function or `PartialActionObserver` instance.
     * @param action
     * @param observer
     */
    public Observe<TSender extends ObjectModel, TEventArgs extends EventArgs>(
        action: { (): Observable<T> },
        observer?: IEvent<TSender, TEventArgs> | PartialActionObserver<T, TEventArgs>): Observable<T>
    {
        let observableResult = <PartialObserver<T>>{};
        if (observer) {
            if (typeof observer == 'function') {
                observableResult = {
                    next    : (result: T) =>
                    {
                        this._NextResult.next(result);
                        observer.bind(this._ThisArg)(this._ThisArg, <TEventArgs>EventArgs.Empty);
                    },
                    complete: () =>
                    {
                        this._ActionResult.next(this._NextResult.getValue());
                    }
                };
            } else {
                if (this._OnProgress && observer.init) {
                    this._OnProgress(this._ThisArg, observer.init());
                }

                observableResult.error = (e: any) =>
                {
                    if (observer.error) {
                        observer.error(e);
                    } else if (this._OnError) {
                        this._OnError(e);
                    } else {
                        console.error(e);
                    }
                };

                observableResult.next = (result: T) =>
                {
                    if (result instanceof Error) {
                        observableResult.error(result);
                        return;
                    }
                    this._NextResult.next(result);
                    if (this._OnProgress && observer.next) {
                        this._OnProgress(this._ThisArg, observer.next(result));
                    }
                };

                observableResult.complete = () =>
                {
                    this._ActionResult.next(this._NextResult.getValue());
                    if (this._OnComplete && observer.complete) {
                        this._OnComplete(this._ThisArg, observer.complete());
                    }
                };
            }
        }
        try {
            this._ActionObservable = action();
        } catch (e) {
            (this._OnError || console.error)(e);
            this._ActionObservable = throwError(e);
        }

        if (observer) {
            this._ActionSubscription = this._ActionObservable.subscribe(observableResult);
        }
        return this._ActionObservable;
    };

    public Cancel()
    {
        this._ActionSubscription && !this._ActionSubscription.closed && this._ActionSubscription.unsubscribe();
    }

    public Reset()
    {
        this._NextResult.next(this._DefaultResult);
        this._ActionResult.next(this._DefaultResult);
    }

    public Equals(other: ActionObserver<T>): boolean
    {
        return super.Equals(other) && this._ActionResult === other._ActionResult;
    }

    /**@inheritDoc*/
    public Dispose(disposing?: boolean)
    {
        if (disposing && !this.IsDisposed) {
            this.Cancel();
            this._NextResult.unsubscribe();
            this._ActionResult.unsubscribe();
        }
        super.Dispose(disposing);
    }
}
