export type ObjectConstructor<T> = new(...args: any[]) => T;

/**
 * Represents the base object of all objects in the model-view-view-model pattern.
 * "If it walks like a duck and it quacks like a duck, then it must be a duck."
 */
export abstract class ObjectModel implements Object
{
    /**
     * Returns a string representation of this instance.
     */
    public toString(): string
    {
        return this.constructor.name;
    }

    /**
     * Determines if this instance equals the provided object model instance.
     * Checks equality by comparing the string representation of this type.
     * @param other
     */
    public Equals(other: ObjectModel): boolean
    {
        // Check self
        if (other == this) {
            return true;
        }

        // Check for null
        if (other == null) {
            return false;
        }

        // Check type
        if (!(other instanceof this.constructor)) {
            return false;
        }

        // Check member
        return this.toString() == other.toString();
    }
}
