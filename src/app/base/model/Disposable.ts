import {EventArgs} from 'base/model/EventArgs';
import {EventDelegate} from 'base/model/EventDelegate';
import {IDisposable} from 'base/model/IDisposable';
import {ObjectModel} from 'base/model/ObjectModel';


/**
 * Represents a disposable object for single threaded environments.
 */
export class Disposable extends ObjectModel implements IDisposable
{
    private _Disposing           = new EventDelegate(this);
    private _Disposed            = new EventDelegate(this);
    private _IsDisposed: boolean = false;

    //<editor-fold desc="IDisposable Members">

    /**@inheritDoc*/
    public Dispose(disposing?: boolean)
    {
        if (arguments[0] === undefined && !this._IsDisposed) {
            this.OnDisposing(EventArgs.Empty);
            this.Dispose(true);
            this.OnDisposed(EventArgs.Empty);
            return;
        }
        this._IsDisposed = true;
    }

    /**@inheritDoc*/
    public get Disposing(): EventDelegate<this>
    {
        return this._Disposing;
    }

    /**@inheritDoc*/
    public get Disposed(): EventDelegate<this>
    {
        return this._Disposed;
    }

    /**@inheritDoc*/
    public get IsDisposed(): boolean
    {
        return this._IsDisposed;
    }

    //</editor-fold>

    protected OnDisposing(args: EventArgs)
    {
        this._Disposing.Invoke(args);
    }

    protected OnDisposed(args: EventArgs)
    {
        this._Disposed.Invoke(args);
    }
}
