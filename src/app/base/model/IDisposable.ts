import {EventDelegate} from 'base/model/EventDelegate';
import {ObjectModel} from 'base/model/ObjectModel';


/**
 * Defines a dispoable object which provides disposal lifecycle events.
 */
export interface IDisposable extends ObjectModel
{
    /**
     * Releases all unmanaged resources of this instance. Please call this method
     * only without providing the 'disposing' parameter (explained within parameter doc).
     * Note: When overridden within an implementing class then call the dispose method of
     * the superclass always at least (after all own disposal logic has been processed).
     * @param {boolean} disposing Note: This parameter is for internal usage only and used within derived
     * implementations to indicate if a regular disposal is in progress prior object finalization.
     */
    Dispose(disposing?: boolean): void;

    /**
     * Raised on before this instance gets disposed.
     */
    Disposing: EventDelegate<this>;

    /**
     * Raised after this instance has been disposed.
     */
    Disposed: EventDelegate<this>;

    /**
     * Returns a value indicating if the current object instance has been disposed.
     */
    IsDisposed: boolean;
}

/**
 * Disposable context definition statement markup for IDisposable implementations.
 * @param {T} resource
 * @param {(resource: T) => void} func
 */
export function using<T extends IDisposable>(resource: T, func: (resource: T) => void)
{
    try {
        func(resource);
    } finally {
        resource.Dispose();
    }
}
