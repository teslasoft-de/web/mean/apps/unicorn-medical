import {
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';

import {ObjectModel} from 'base/model';
import {IViewModel} from 'base/view-model';


export {SimpleChanges};

/**
 * Represents the object base for all ngx components within the model-view-view-model pattern.
 * Keep this as clean as possible from specialized code to be able to migrate to future ngx versions.
 */
export abstract class ComponentBase<TViewModel extends IViewModel<TModel>, TModel extends ObjectModel = TModel>
    implements OnInit,
               OnDestroy,
               AfterViewInit,
               AfterViewChecked,
               AfterContentInit,
               AfterContentChecked,
               OnChanges
{
    /**
     * Returns the view-model instance associated with this component instance.
     */
    @Output('vm')
    public get vm(): TViewModel
    {
        return this._vm;
    }

    @Input('model')
    public set Model(value: TModel)
    {
        this.vm.Model = value;
    }

    @Output('model')
    public get model(): TModel
    {
        return this.vm.Model;
    }

    protected constructor(private _vm: TViewModel) {}

    /** @inheritDoc */
    public ngOnInit()
    {
        this._vm.Init && this._vm.Init();
    }

    /** @inheritDoc */
    public ngOnDestroy(): void
    {
        this._vm.Destroy && this._vm.Destroy();
    }

    /** @inheritDoc */
    public ngAfterViewInit(): void
    {
        this._vm.ViewInit && this._vm.ViewInit();
    }

    public ngAfterViewChecked(): void
    {
        this._vm.ViewChecked && this._vm.ViewChecked();
    }

    public ngAfterContentInit(): void
    {
        this._vm.ContentInit && this._vm.ContentInit();
    }

    public ngAfterContentChecked(): void
    {
        this._vm.ContentChecked && this._vm.ContentChecked();
    }

    public ngOnChanges(changes: SimpleChanges): void
    {
        this._vm.Changes && this._vm.Changes(changes);
    }
}
