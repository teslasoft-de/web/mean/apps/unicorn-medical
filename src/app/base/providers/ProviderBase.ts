import {ClassProvider, FactoryProvider, Injector} from '@angular/core';

import {ObjectConstructor, ObjectModel} from 'base/model';
import {ServiceBase} from 'base/services';
import {environment} from 'environments/environment';


/**
 * Configures the factory provider to return the service factory configuration.
 */
export interface IServiceProvider
{
    /**
     * An injection token. (Typically an instance of `Type` or `InjectionToken`, but can be `any`).
     */
    provide?: ObjectConstructor<ServiceBase<any>>;

    /**
     * A list of `token`s which need to be resolved by the injector. The list of values is then
     * used as arguments to the `useFactory` function.
     */
    deps?: any[];
}

/**
 * Represents the base object for the provider in the service provider pattern.
 */
export abstract class ProviderBase extends ObjectModel
{
    /**
     * Returns the factory configuration provided by the type on which this function was invoked on.
     * @param provider
     * @param provide
     */
    public static Factory(provider: ProviderBase = new (<any>this)(), provide: ObjectConstructor<ServiceBase<any>>): FactoryProvider
    {
        const factoryProvider = environment.production
            ? provider.ProductionFactory()
            : provider.DevelopmentFactory();

        if (!factoryProvider.provide) {
            factoryProvider.provide = provide;
        }

        return {
            provide   : factoryProvider.provide,
            useFactory: ((...deps: any[]) =>
            {
                return new (factoryProvider.provide.bind(null, ...deps));
            }),
            deps      : factoryProvider.deps || []
        };
    }

    /**
     * Returns the staging configuration provided by the type on which this function was invoked on.
     */
    public static get Staging(): ClassProvider
    {
        const providerBase      = new (<any>this)();
        const productionFactory = providerBase.ProductionFactory();
        const factoryProvider   = environment.production
            ? productionFactory
            : providerBase.DevelopmentFactory();

        return {
            provide : productionFactory.provide,
            useClass: factoryProvider.provide
        };
    }

    public Provide(provide: ObjectConstructor<ServiceBase<any>>): ServiceBase<any>
    {
        const factoryProvider = (<typeof ProviderBase>this.constructor).Factory(this, provide);

        if (!factoryProvider.provide) {
            factoryProvider.provide = provide;
        }

        const injector = Injector.create({
            providers: [{
                provide   : factoryProvider.provide,
                useFactory: factoryProvider.useFactory,
                deps      : factoryProvider.deps
            }],
            parent   : this._parentInjector
        });

        return injector.get(factoryProvider.provide);
    }

    protected constructor(protected _parentInjector: Injector)
    {
        super();
    }

    public abstract ProductionFactory(): IServiceProvider;

    public abstract DevelopmentFactory(): IServiceProvider;
}
