import {HttpResponse} from '@angular/common/http';
import {Observable, of as observableOf} from 'rxjs';
import {catchError, publish, refCount} from 'rxjs/operators';

import {ObjectModel} from 'base/model';


export class IService {}

/**
 * Represents the base object for the service in the service provider pattern.
 */
export abstract class ServiceBase<T> extends ObjectModel implements IService
{
    protected _BackgroundWorkers = new Map<string, Observable<T>>();

    public get BackgroundWorkers()
    {
        return this._BackgroundWorkers;
    }

    public get IsSearchInProgress()
    {
        return !!this._BackgroundWorkers.size;
    }

    protected CreateBackgroundWorker(observable: Observable<T>, workerName: string): Observable<T>
    {
        const backgroundWorker = this.Multicast(observable);
        backgroundWorker.subscribe({
            complete: () => this._BackgroundWorkers.delete(workerName)
        });
        this._BackgroundWorkers.set(workerName, backgroundWorker);
        return backgroundWorker;
    }

    /**
     *
     * @param observable
     * @constructor
     */
    protected Multicast<T>(observable: Observable<T>)
    {
        return observable.pipe(
            catchError((err: HttpResponse<JSON>) => observableOf<any>(err)),
            publish(),
            refCount()
        );
    }
}
