import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from 'app';
import {AppRoutingModule} from 'app/core/routing';
import {DashboardComponent, LatestSearchComponent} from 'app/dashboard/view';
import {SearchComponent, StackOverflowSearchComponent, WeatherSearchComponent} from 'app/search/view';
import {LayoutModule} from 'core/layout';
import {AccordionModule, AlertModule, CarouselModule, ProgressbarModule, TabsModule} from 'ngx-bootstrap';
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        LatestSearchComponent,
        SearchComponent,
        StackOverflowSearchComponent,
        WeatherSearchComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        LayoutModule,
        AccordionModule.forRoot(),
        AlertModule.forRoot(),
        CarouselModule.forRoot(),
        ProgressbarModule.forRoot(),
        TabsModule.forRoot(),
        NgxSpinnerModule,
        AppRoutingModule
    ],
    bootstrap   : [AppComponent]
})
export class AppModule {}
