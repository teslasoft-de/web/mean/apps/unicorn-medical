import {browser, by, element} from 'protractor';


export class UnicornMedicalPage
{
    public navigateTo()
    {
        return browser.get('/');
    }

    public getParagraphText()
    {
        return element(by.css('.navbar-brand')).getText();
    }
}
